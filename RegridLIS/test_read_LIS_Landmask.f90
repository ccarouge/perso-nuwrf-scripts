program test_read_LIS_Landmask
implicit none
REAL, DIMENSION(:,:), ALLOCATABLE :: A
open(unit=17,file='/srv/ccrc/data15/z3368490/LIS_PARAMS/UMD/1KM/landmask_UMD.1gd4r', &
                   status='old',access='direct',form='unformatted', &
                   convert='big_endian',recl=36000*15000)  
allocate(A(36000,15000)) 
read(17,rec=1)A(:,:)
print *,MAXVAL(A)
print *,MINVAL(A)
!print *, A
close(17)
end program test_read_LIS_Landmask
