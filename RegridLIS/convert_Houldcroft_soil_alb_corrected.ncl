
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

; Script to convert the background soil albedo developped by Houldcroft et al. (2009) for use in LIS
; see the "00README" at "/srv/ccrc/data06/z3381484/Houldcroft-MODIS-Soil-Albedo/modis_soil_albedo_cmg_v3" for info on the data

begin

; Doing the interpolation or reading from file
interp = False

; Missing value
missval = -9999 ; default for regridding routines

;define dir to output and file name
diro = "/srv/ccrc/data15/z3368490/LIS_PARAMS/UMD/1KM/"
filo = "ccrc_soil_alb_int_short.1gd4r"
system("/bin/rm -f " + diro + filo)

; Mapping resources
res = True
res@cnFillOn            = True          ; turn on color
res@cnLinesOn           = False
res@cnLineLabelsOn     = False
res@cnFillMode="RasterFill"
res@gsnSpreadColors     = True          ; use full range of colormap
;res@gsnDraw = False
;res@gsnFrame = False

res@gsnAddCyclic = False

; set option to Little-Endian for reading
setfileoption("bin","ReadByteOrder","LittleEndian")

;define grid as per 00README file provided
lat = fspan(89.975,89.975+(-0.05*3599),3600)
lon = fspan(-179.975,-179.975+(0.05*7199),7200) 

;define LIS lat and lon
LIS_lon = fspan(-179.995,-179.995+(35999*0.01),36000)
LIS_lat = fspan(-59.995,-59.995+(14999*0.01),15000)
LIS_lat@units = "degrees-north"
LIS_lon@units = "degrees-east"

if (interp) then
; open the file, as in the paper, we are using the white sky total shortwave albedo
  white_sky_soil_alb_integrated_shortwave = fbindirread("/srv/ccrc/data06/z3381484/Houldcroft-MODIS-Soil-Albedo/modis_soil_albedo_cmg_v3/modis_wsa_soil_bb3_cmg",0,(/3600,7200/),"float")

;interpolate to LIS grid, all lats and longs must be monotically increasing
; native lat is monotically decrease, reverse order here
  lat1 = lat(::-1)
  delete(lat)
; but also need to reverse lat dim in array
  soil_alb_rev = white_sky_soil_alb_integrated_shortwave(::-1,:)
  delete(white_sky_soil_alb_integrated_shortwave)
  soil_alb_rev@_FillValue = -99
  print("Interpolation")
  soil_alb_rev_LIS_grid = linint2(lon,lat1,soil_alb_rev,False,LIS_lon,LIS_lat,0)
; Save interpolated array to file
  tfile = addfile("~/S/temp_alb.nc","c")
  tfile->alb = soil_alb_rev_LIS_grid
  delete(soil_alb_rev)
else
  ; Read array from file
  tfile = addfile("~/S/temp_alb.nc","r")
  soil_alb_rev_LIS_grid = tfile->alb
  delete(tfile)
end if
if False then
; Plot the unmasked array for check.
; Gives the lat and lon arrays as coordinate arrays for plot
soil_alb_rev_LIS_grid!0 = "lat"
soil_alb_rev_LIS_grid!1 = "lon"
soil_alb_rev_LIS_grid&lat = LIS_lat
soil_alb_rev_LIS_grid&lon = LIS_lon

res@mpMinLatF = min(LIS_lat)
res@mpMaxLatF = max(LIS_lat)
res@mpMinLonF = min(LIS_lon)
res@mpMaxLonF = max(LIS_lon)

print("Plot interpolated")
soil_alb_plot_miss = new(10,graphic)
wks_soil_alb_miss  = gsn_open_wks("pdf","test_Houldcroft_soil_alb_miss_single")
setvalues NhlGetWorkspaceObjectId()
  "wsMaximumSize" : 500000000
end setvalues
gsn_define_colormap(wks_soil_alb_miss,"BlAqGrYeOrRe")
soil_alb_rev_LIS_grid@_FillValue = -99
; Plot by bands since too big for memory.
do i=0,9
  step=dimsizes(LIS_lon)/10
  is= i*step
  ie= (i+1)*step
  soil_alb_plot_miss(i) = gsn_csm_contour_map_ce(wks_soil_alb_miss,soil_alb_rev_LIS_grid(:,is:ie-1),res)
end do
;frame(wks_soil_alb_miss)
delete(wks_soil_alb_miss)
end if

; now mask the array, same as input data for LIS
; get land-mask from a default LIS input file provided by NASA:
print("Mask array")
setfileoption("bin","ReadByteOrder","BigEndian")
all_alb_def = fbindirread("/srv/ccrc/data15/z3368490/LIS_PARAMS/UMD/1KM/landmask_UMD.1gd4r",0,(/15000,36000/),"float")
soil_alb_rev_LIS_grid = where(all_alb_def.lt.0.5,missval,soil_alb_rev_LIS_grid)
soil_alb_rev_LIS_grid_1d = ndtooned(soil_alb_rev_LIS_grid)
delete(soil_alb_rev_LIS_grid)

;all_alb_def_1d = ndtooned(all_alb_def)
;delete(all_alb_def)
;ind_miss = ind(all_alb_def_1d .eq. 0)
;delete(all_alb_def_1d)

;mask the array
;soil_alb_rev_LIS_grid_1d = ndtooned(soil_alb_rev_LIS_grid)
;delete(soil_alb_rev_LIS_grid)
;soil_alb_rev_LIS_grid_1d(ind_miss) = missval

; The soil albedo data has FillValues over densely forested regions, Amazon, Congo, and some regions of East Asia.
; Here we replace with soil albedo values from Wilson and Henderson-Sellers (1985), Int. J. Clim, Vol 5, 119-142
; depending on the soil color (see Table IX of the paper).
; We first get soil colors provided with LIS (but we are not currenlty using with CABLE):
print("Fill forested regions")
soil_col_def = fbindirread("/srv/ccrc/data15/z3368490/LIS_PARAMS/UMD/1KM/soilcolor_FAO.1gd4r",0,(/15000,36000/),"float")
soil_col_def_1d = ndtooned(soil_col_def)
delete(soil_col_def)
ind_99 = ind(ismissing(soil_alb_rev_LIS_grid_1d))
color_at_missing_points = soil_col_def_1d(ind_99)

;replace missing values with global estimates
print("replace missing")

; Use where instead of loop
soil_alb_rev_LIS_grid_1d(ind_99) = 0.26
print("replace missing: <=7")
soil_alb_rev_LIS_grid_1d(ind_99) = where(color_at_missing_points.le.7,0.15,soil_alb_rev_LIS_grid_1d(ind_99))
print("replace missing: <=4")
soil_alb_rev_LIS_grid_1d(ind_99) = where(color_at_missing_points.le.4,0.11,soil_alb_rev_LIS_grid_1d(ind_99))

;now re-grid
print("to 2D")
new_soil_alb = onedtond(soil_alb_rev_LIS_grid_1d,(/15000,36000/))
delete(soil_alb_rev_LIS_grid_1d)

; write to binary format
print("Write to file")
setfileoption ("bin", "WriteByteOrder", "BigEndian")
print(dimsizes(new_soil_alb))
printVarSummary(new_soil_alb)
do i=0,14999
  fbindirwrite(diro+filo,new_soil_alb(i,:))
end do

; test plotting
print("Plot result")
new_soil_alb!0 = "lat"
new_soil_alb!1 = "lon"
new_soil_alb&lat = LIS_lat
new_soil_alb&lon = LIS_lon
; Plot by band since too expensive in memory
soil_alb_plot_nomiss = new(10,graphic)
wks_soil_alb_nomiss  = gsn_open_wks("pdf","test_Houldcroft_soil_alb_nomiss_single")
gsn_define_colormap(wks_soil_alb_nomiss,"BlAqGrYeOrRe")
setvalues NhlGetWorkspaceObjectId()
  "wsMaximumSize" : 500000000
end setvalues
new_soil_alb@_FillValue = missval
do i=0,9
  step=dimsizes(LIS_lon)/10
  is= i*step
  ie= (i+1)*step
  soil_alb_plot_nomiss(i) = gsn_csm_contour_map_ce(wks_soil_alb_nomiss,new_soil_alb(:,is:ie-1),res)
end do
;frame(wks_soil_alb_nomiss)
delete(wks_soil_alb_nomiss)

res@gsnDraw = False
res@gsnFrame = False
soil_alb_plot = new(20,graphic)
wks_soil_alb  = gsn_open_wks("pdf","test_Houldcroft_soil_alb_panel")
setvalues NhlGetWorkspaceObjectId()
  "wsMaximumSize" : 500000000
end setvalues
do i=0,19,2
  soil_alb_plot(i) = soil_alb_plot_nomiss(i/2)
  soil_alb_plot(i+1) = soil_alb_plot_miss(i/2)
end do
resP = True
do i=0,19,4
  gsn_panel(wks_soil_alb,soil_alb_plot(i:i+3),(/2,2/),resP)
end do
end
