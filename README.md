Scripts to regrid the 0.001x0.001 degree LIS dataset to other grid resolutions.

Scripts to setup and run NUWRF v6-3.4.1

A complete manual can be found [here](http://climate-cms.unsw.wikispaces.net/NU-WRF).
