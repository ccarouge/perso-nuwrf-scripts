#!/bin/csh

#
#script to create boundary condition files from CSIRO mk3.5 history files
#need to have histheights.f90 compiled
#then need to compile mk3.5toInter
#then run these decks
#
#put all the decks in the Inter directory

if (${NOFIRST} && ${NOSECOND}) then
    #test to see if the previous wrfbdy file exists - if not then exit
    if ( -e ${RSYNC_PATH}/${BDYDATA_PATH}/wrfbdy_d01_${PYEAR}-${PMONTH} ) then
    else
	echo "${RSYNC_PATH}/${BDYDATA_PATH}/wrfbdy_d01_${PYEAR}-${PMONTH} does not exist"
	exit
    endif
endif

if (${FIRST} && ${COUPLED}) then
    cd ${RSYNC_PATH}/${DECK_PATH}
    # Update geo_em file with the data from LIS
    ncl update_static.ncl
endif

#go to the directory where intermediate files are created
cd ${MAKEINTER_DIR}

#link history files (year zero is 1700)
cp ${DATA_DIR}/histb.m${SMONTH}.y${HYEAR}.B35.nc .

#run mk3.5toInter
./mk3.5toInter2 histb.m${SMONTH}.y${HYEAR}.B35.nc

#check if the zero time step of a fictional last day+1 of the month exists
#rename it if so (error from CSIRO!)
if ( -e MK35:${SYEAR}-${SMONTH}-${RDAY}_00 ) then
  mv MK35:${SYEAR}-${SMONTH}-${RDAY}_00 MK35:${NYEAR}-${NMONTH}-01_00
endif

if (${LEAP_YEAR}) then
    # Add leap year day
    cp MK35:${SYEAR}-02-28_00 MK35:${SYEAR}-02-29_00
    cp MK35:${SYEAR}-02-28_06 MK35:${SYEAR}-02-29_06
    cp MK35:${SYEAR}-02-28_12 MK35:${SYEAR}-02-29_12
    cp MK35:${SYEAR}-02-28_18 MK35:${SYEAR}-02-29_18
endif

#remove the history file
rm histb.m${SMONTH}.y${HYEAR}.B35.nc MK35:${PYEAR}-${PMONTH}-*

if (${NOFIRST}) then

    #change to the WPS directory
    cd ${WPS_DIR}
    echo move to ${WPS_DIR}

#link the intermediate files and run metgrid
    rm MK35* met_em.d0*

    ln -s ${MAKEINTER_DIR}/MK35* .
    echo link intermediate files


    #write the namelist.wps
    cat >! ./namelist.wps << EOF_namelist_wps
&share
 wrf_core = 'ARW',
 max_dom = ${max_dom},
 start_date = '${SYEAR}-${SMONTH}-01_00:00:00', '${SYEAR}-${SMONTH}-01_00:00:00', 
 end_date   = '${NYEAR}-${NMONTH}-01_00:00:00', '${NYEAR}-${NMONTH}-01_00:00:00', 
 interval_seconds = 21600,
 io_form_geogrid = 2,
 opt_output_from_geogrid_path = '${RSYNC_PATH}/${GEO_PATH}',
 debug_level = 0,
/

&geogrid
 parent_id         = ${PARENT_ID_AR}
 parent_grid_ratio = ${PARENT_GRID_RATIO_AR}
 i_parent_start    = ${I_PARENT_START_AR}
 j_parent_start    = ${J_PARENT_START_AR}
 e_we          = ${E_WE_AR}
 e_sn          = ${E_SN_AR}
 geog_data_res = '10m','2m',
 dx = ${DX},
 dy = ${DY},
 map_proj =  'lambert',
 ref_lat   = ${REF_LAT},
 ref_lon   = ${REF_LON},
 truelat1  = ${TRUELAT1},
 truelat2  = ${TRUELAT2},
 stand_lon = ${STAND_LON},
 geog_data_path = '/srv/ccrc/data02/z3236814/data/WRF_geog/geog',
/

&ungrib
 out_format = 'WPS',
 prefix = 'NNRPSFC',
/

&metgrid
 fg_name = 'MK35',
 io_form_metgrid = 2,
 opt_metgrid_tbl_path = '${MAKEINTER_DIR}'
 opt_output_from_metgrid_path = '${RSYNC_PATH}/${BDYDATA_PATH}'
/

&mod_levs
 press_pa = 201300 , 200100 , 100000 ,
             95000 ,  90000 ,
             85000 ,  80000 ,
             75000 ,  70000 ,
             65000 ,  60000 ,
             55000 ,  50000 ,
             45000 ,  40000 ,
             35000 ,  30000 ,
             25000 ,  20000 ,
             15000 ,  10000 ,
              5000 ,   1000
/
EOF_namelist_wps

    # Copy namelist.wps in output dir for archiving
    cp namelist.wps ${RSYNC_PATH}/${BDYDATA_PATH}/namelist.wps.${SYEAR}_${SMONTH}

    ./metgrid.exe
    echo run metgrid


    #now run real.exe on these metgrid files
    cd  ${CCRCRUN_WRF}

    rm wrfrst_d0*
    rm wrfbdy_d0?
    rm wrflowinp_d0?
    rm wrfout_d0*
    
    rm met_em.d0*
    ln -s ${RSYNC_PATH}/${BDYDATA_PATH}/met_em.d0* .
    echo 'link met_em files in ${CCRCRUN_WRF}'




    #write the namelist.input
    cat >! ./namelist.input << EOF_namelist
&time_control
 run_days                            = ${RUNDAYS},
 run_hours                           = 0,
 run_minutes                         = ${OLAP},
 run_seconds                         = 0,
 start_year                          =${SYEAR},${SYEAR},${SYEAR},
 start_month                         = ${SMONTH},${SMONTH},${SMONTH},
 start_day                           = ${SDAY},${SDAY},${SDAY},
 start_hour                          = 00,   00,   00,
 start_minute                        = 00,   00,   00,
 start_second                        = 00,   00,   00,
 end_year                            = ${NYEAR}, ${NYEAR}, ${NYEAR}
 end_month                           = ${NMONTH}, ${NMONTH}, ${NMONTH},
 end_day                             = ${NDAY}, ${NDAY}, ${NDAY},
 end_hour                            = 00,   00,   12,
 end_minute                          = ${OLAP},   00,   00,
 end_second                          = 00,   00,   00,
 interval_seconds                    = 21600
 input_from_file                     = .true.,.true.,.false.,
 history_interval                    = 180, 180,   60,
 frames_per_outfile                  = 1000, 36, 1000,
 restart                             = ${ISRESTART}
 restart_interval                    = ${RES_WRF}
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 0
 auxinput4_inname                    = "wrflowinp_d<domain>"
 auxinput4_interval                  = 360,360,
 io_form_auxinput4                   = 2
 auxhist3_outname                    = 'wrfxtrm_d<domain>_<date>'
 io_form_auxhist3                    = 2
 auxhist3_interval                   = 1440, 1440
 frames_per_auxhist3                 = ${RUNDAYS}, ${RUNDAYS}
 auxhist4_outname                    = 'wrfrain_d<domain>_<date>'
 io_form_auxhist4                    = 2
 auxhist4_interval                   = 1440,1440
 frames_per_auxhist4                 = ${RUNDAYS}, ${RUNDAYS}
 auxhist5_outname                    = 'wrf24h_d<domain>_<date>'
 io_form_auxhist5                    = 2
 auxhist5_interval                   = 1440,1440
 frames_per_auxhist5                 = ${RUNDAYS}, ${RUNDAYS}
 auxhist5_begin_h                    = 0,0
 /

 &diagnostics
 clwrf_variables                     = 1, 1
 max_window                          = 10
 max_wind_10m                        = 1, 1
 /

 &domains
 time_step                           = ${WRF_ts},
 time_step_fract_num                 = 0,
 time_step_fract_den                 = ${max_dom},
 max_dom                             = 2,
 s_we                                = 1,     1,     1,
 e_we                                = ${E_WE_AR}
 s_sn                                = 1,     1,     1,
 e_sn                                = ${E_SN_AR}
 s_vert                              = 1,     1,     1,
 e_vert                              = 30,    30,    28,
 eta_levels = 1.00,0.995,0.99,0.98,0.97,0.96,0.94,0.92,0.89,0.86,0.83,0.80,0.77,0.72,0.67,0.62,0.57,0.52,0.47,0.42,0.37,0.32,0.27,0.22,0.17,0.12,0.07,0.04,0.02,0.00
 num_metgrid_levels                  = 19
 dx                                  = ${DX_AR}
 dy                                  = ${DY_AR}
 grid_id                             = ${GRID_ID_AR}
 parent_id                           = ${PARENT_ID_AR}
 i_parent_start                      = ${I_PARENT_START_AR}
 j_parent_start                      = ${J_PARENT_START_AR}
 parent_grid_ratio                   = ${PARENT_GRID_RATIO_AR}
 parent_time_step_ratio              = 1,     5,     3,
 feedback                            = 0,
 smooth_option                       = 0
 /

 &physics
 mp_physics                          = 4,     4,     3,
 mp_zero_out                         = 2
 mp_zero_out_thresh                  = 1.e-8
 ra_lw_physics                       = 1,     1,     1,
 ra_sw_physics                       = 1,     1,     1,
 radt                                = 10,    10,    10,
 cam_abs_freq_s                      = 10800
 levsiz                              = 59
 paerlev                             = 29
 cam_abs_dim1                        = 4
 cam_abs_dim2                        = 28
 sf_sfclay_physics                   = 1,     1,     1,
 sf_surface_physics                  = 2,     2,     1,
 bl_pbl_physics                      = 1,     1,     1,
 bldt                                = 0,     0,     0,
 cu_physics                          = 1,     1,     0,
 cudt                                = 5,     0,     5,
 isfflx                              = 1,
 surface_input_source                = 1,
 num_soil_layers                     = 4,
 sf_urban_physics                    = 0,
 sst_update                          = 1,
 usemonalb                           = .true.
 slope_rad                           = 1,
 /

 &fdda
 /

 &dynamics
 rk_ord                              = 3,
 w_damping                           = 0,
 diff_opt                            = 1,
 km_opt                              = 4,
 diff_6th_opt                        = 0,
 diff_6th_factor                     = 0.12,
 base_temp                           = 290.
 damp_opt                            = 3,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.05,   0.05,   0.01
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1, 1, 1,
 scalar_adv_opt                      = 1, 1, 1,
 /

 &bdy_control
 spec_bdy_width                      = 5,
 spec_zone                           = 1,
 relax_zone                          = 4,
 specified                           = .true., .false.,.false.,
 nested                              = .false., .true., .true.,
 /

 &grib2
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /
EOF_namelist


    # Copy namelist.wps in output dir for archiving
    cp namelist.input ${RSYNC_PATH}/${BDYDATA_PATH}/namelist.input.${SYEAR}_${SMONTH}

    ./real.exe
    echo 'run real'


    cp wrfbdy_d01${RSYNC_PATH}/${BDYDATA_PATH}/wrfbdy_d01_${SYEAR}-${SMONTH}

    # Note: this loop only works for max_dom < 10.
    set ite=1
    while ( $ite <= ${max_dom} )
	cp wrflowinp_d0${ite} ${RSYNC_PATH}/${BDYDATA_PATH}/wrflowinp_d0${ite}_${SYEAR}-${SMONTH}
	cp wrfinput_d0${ite} ${RSYNC_PATH}/${BDYDATA_PATH}/wrfinput_d0${ite}_${SYEAR}-${SMONTH}
	    
	@ ite++
    end
	
    rm met_em.d0* ${RSYNC_PATH}/${BDYDATA_PATH}/met_em.d0*
endif

cd ${RSYNC_PATH}/${DECK_PATH}
./getbdy_${INI_FORMAT}_${NYEAR}_${NMONTH}.deck >& log_${NYEAR}_${NMONTH} &

